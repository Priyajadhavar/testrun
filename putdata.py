import json
import boto3
client=boto3.resource("dynamodb")
table=client.Table("testrun")

def lambda_handler(event, context):
    
    id=event["id"]
    tstatus=event["tstatus"]
    params = {
        'testrun_id': id
    }

    response = table.update_item(
        Key=params,
        UpdateExpression="SET testrunstatus = :s",
        ExpressionAttributeValues={
            ':s': tstatus
        },
        ReturnValues="UPDATED_NEW"
    )
 

    return {
        'statusCode': 200,
        'body': json.dumps({'msg': 'data updated'})
    }
    
   
    

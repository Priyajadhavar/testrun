import json
import boto3
import uuid
from datetime import datetime
import time
from decimal import Decimal

ERROR_RESPONSE = {
    'statusCode': 405,
    'body': json.dumps('Oops, something went wrong!')
}
FORBIDDEN_RESPONSE = {
    'statusCode': 403,
    'body': json.dumps('Forbidden')
}



client=boto3.resource("dynamodb")

# Convert Decimal to float
class DecimalEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Decimal):
            return float(obj)
        return json.JSONEncoder.default(self, obj)

def run_handler(event, context):
    print(event)
    
    if event.get('httpMethod') == 'GET' and event['pathParameters']:
        table=client.Table("testrun")

        testrun_id = event['pathParameters']['testrun_id']
        result=table.get_item(Key={"testrun_id":testrun_id})
    
        OK_RESPONSE = {
                "statusCode": 200,
                "body": json.dumps(result["Item"], cls=DecimalEncoder)
            }
        return OK_RESPONSE
   
    if event.get('httpMethod') == 'GET':
        table=client.Table("testrun")
        result = table.scan()
    
        OK_RESPONSE = {
                "statusCode": 200,
                "body": json.dumps(result["Items"], cls=DecimalEncoder)
            }
        return OK_RESPONSE
        
        
    if event.get('httpMethod') == 'POST' and event.get('body'):
        table=client.Table("testrun")
        data = json.loads(event["body"])
        testrun_id = str(uuid.uuid1())
        data["testrun_id"] = testrun_id
        data["submit_date"] = str(time.time())
        data["start_date"] = str(time.time())
        data["end_date"] = ""

        table.put_item(Item=data)

        OK_RESPONSE = {
                "statusCode": 201,
                "body": json.dumps({"testrun_id":testrun_id})
            }
        return OK_RESPONSE
 
    if event.get('httpMethod') == 'PUT' and event.get('body'):
        table=client.Table("testrun")
        testrun_id = str(event['pathParameters']['testrun_id'])
        item = json.loads(event["body"])
        print(item)
        # Use update_item insted of below line. 
        # Instead of getting an object and saving modified, update in the table directly.
        result=table.get_item(Key={"testrun_id":testrun_id})
        print(result["Item"])
        item = {**result["Item"], **item}
        
        table.put_item(Item=item)

        OK_RESPONSE = {
                "statusCode": 200,
                "body": json.dumps({"testrun_id":testrun_id})
            }
        return OK_RESPONSE
  
  
        
    return ERROR_RESPONSE



#sorry sir but i did not get  understand  how to pass the testcases can we connect if you have time
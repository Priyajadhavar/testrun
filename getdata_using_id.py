import json
import boto3
client=boto3.resource("dynamodb")
table=client.Table("testrun")

def lambda_handler(event, context):
    testrunid=event['testrunid']
    result=table.get_item(Key={"testrun_id":testrunid})
    
    
    return result["Item"]
    
   

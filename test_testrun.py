
import sys, os
import pprint
myPath = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, myPath + '/../')
from botocore.exceptions import ClientError
pprint.pprint(sys.path)
import pytest
from testrun import *
import json
from moto import *
from mock import MagicMock, patch
#from lambda_function_context import *
import boto3
# os.environ['AWS_DEFAULT_REGION']= 'us-east-1'
EVENT_JSON = {'resource': '/v1/testrun', 'path': '/v1/testrun', 'httpMethod': 'GET', 'headers': {'Accept': '*/*', 'Accept-Encoding': 'gzip, deflate, br', 'CloudFront-Forwarded-Proto': 'https', 'CloudFront-Is-Desktop-Viewer': 'true', 'CloudFront-Is-Mobile-Viewer': 'false', 'CloudFront-Is-SmartTV-Viewer': 'false', 'CloudFront-Is-Tablet-Viewer': 'false', 'CloudFront-Viewer-Country': 'IN', 'Host': 'x4ocrus97f.execute-api.us-east-1.amazonaws.com', 'Postman-Token': '48937261-2d3b-45d6-8366-57e6f3184aa6', 'testsu-ApiGa-hGwTlON0l1wZ': '0604aea7-7c28-4d15-8252-05cc2e03d10f', 'User-Agent': 'PostmanRuntime/7.26.8', 'Via': '1.1 f4810eebb41fa49d77440fd8bc488c56.cloudfront.net (CloudFront)', 'X-Amz-Cf-Id': 'gigfR19w0hWktCrzmIxJfYclTLLEd2axrYWSXOPrZfWcB1hrZlTlUg==', 'X-Amzn-Trace-Id': 'Root=1-60f7aeef-6d2c6aea677aeee40a8db64a', 'x-api-key': 'pPhB8p1Lqn5vV3A6W8Z0M76opVHbyxqnagCHoBM3', 'X-Forwarded-For': '103.200.107.8, 130.176.20.139', 'X-Forwarded-Port': '443', 'X-Forwarded-Proto': 'https'}, 'multiValueHeaders': {'Accept': ['*/*'], 'Accept-Encoding': ['gzip, deflate, br'], 'CloudFront-Forwarded-Proto': ['https'], 'CloudFront-Is-Desktop-Viewer': ['true'], 'CloudFront-Is-Mobile-Viewer': ['false'], 'CloudFront-Is-SmartTV-Viewer': ['false'], 'CloudFront-Is-Tablet-Viewer': ['false'], 'CloudFront-Viewer-Country': ['IN'], 'Host': ['x4ocrus97f.execute-api.us-east-1.amazonaws.com'], 'Postman-Token': ['48937261-2d3b-45d6-8366-57e6f3184aa6'], 'testsu-ApiGa-hGwTlON0l1wZ': ['0604aea7-7c28-4d15-8252-05cc2e03d10f'], 'User-Agent': ['PostmanRuntime/7.26.8'], 'Via': ['1.1 f4810eebb41fa49d77440fd8bc488c56.cloudfront.net (CloudFront)'], 'X-Amz-Cf-Id': ['gigfR19w0hWktCrzmIxJfYclTLLEd2axrYWSXOPrZfWcB1hrZlTlUg=='], 'X-Amzn-Trace-Id': ['Root=1-60f7aeef-6d2c6aea677aeee40a8db64a'], 'x-api-key': ['pPhB8p1Lqn5vV3A6W8Z0M76opVHbyxqnagCHoBM3'], 'X-Forwarded-For': ['103.200.107.8, 130.176.20.139'], 'X-Forwarded-Port': ['443'], 'X-Forwarded-Proto': ['https']}, 'queryStringParameters': None, 'multiValueQueryStringParameters': None, 'pathParameters': None, 'stageVariables': None, 'requestContext': {'resourceId': 'v8ermh', 'resourcePath': '/v1/testrun', 'httpMethod': 'GET', 'extendedRequestId': 'CzhFeGuJIAMFyBw=', 'requestTime': '21/Jul/2021:05:21:51 +0000', 'path': '/dev/v1/testrun', 'accountId': '897708493501', 'protocol': 'HTTP/1.1', 'stage': 'dev', 'domainPrefix': 'x4ocrus97f', 'requestTimeEpoch': 1626844911640, 'requestId': 'c174c823-e437-4c26-aae8-a958a3183edc', 'identity': {'cognitoIdentityPoolId': None, 'cognitoIdentityId': None, 'apiKey': 'pPhB8p1Lqn5vV3A6W8Z0M76opVHbyxqnagCHoBM3', 'principalOrgId': None, 'cognitoAuthenticationType': None, 'userArn': None, 'apiKeyId': 'fznpe2q91d', 'userAgent': 'PostmanRuntime/7.26.8', 'accountId': None, 'caller': None, 'sourceIp': '103.200.107.8', 'accessKey': None, 'cognitoAuthenticationProvider': None, 'user': None}, 'domainName': 'x4ocrus97f.execute-api.us-east-1.amazonaws.com', 'apiId': 'x4ocrus97f'}, 'body': None, 'isBase64Encoded': False}
RESULT = [ { "end_date": "", "status": "submitted", "version_control_url": "http://testUrl2", "testrun_name": "MyApp2", "submit_date": "1626672940.5121121", "testrun_id": "275ab5eb-e853-11eb-8d3f-4b9c48f607a6", "test_code_s3_object_key": "SampleKey2", "org_id": "6826fcab-e22c-11eb-8abc-b94b3b7405d6", "user_id": "40543869-e22c-11eb-9982-b94b3b7405d6", "start_date": "1626672940.512124", "testrun_command": "TestRunCommand2", "testrun_output_dir": "output_dir2", "aut_url": "http://aut_url2", "git_username": "username2", "job_type": "dev", "browser_under_test": [ { "browser_name": "Chrome", "browser_resolution": "840x940", "browser_version": "92.0", "browser_count": 1.0 } ] }, { "end_date": "", "status": "submitted", "version_control_url": "http://testUrl", "testrun_name": "MyApp", "submit_date": "1626022948.8360364", "testrun_id": "c61fef2e-e269-11eb-84e3-95f449c76441", "test_code_s3_object_key": "SampleKey", "org_id": "6826fcab-e22c-11eb-8abc-b94b3b7405d6", "user_id": "40543869-e22c-11eb-9982-b94b3b7405d6", "start_date": "1626022948.836058", "testrun_command": "TestRunCommand", "testrun_output_dir": "output_dir", "aut_url": "http://aut_url", "git_username": "username", "job_type": "dev", "browser_under_test": [ { "browser_name": "Chrome", "browser_resolution": "840x940", "browser_version": "91.0", "browser_count": 10.0 } ] }, { "end_date": "", "status": "completed", "version_control_url": "http://testUrl2", "testrun_name": "MyApp2", "submit_date": "1626071052.92347", "testrun_id": "c6652ad8-e2d9-11eb-9c98-9394674b703b", "test_code_s3_object_key": "SampleKey2", "org_id": "6826fcab-e22c-11eb-8abc-b94b3b7405d6", "user_id": "40543869-e22c-11eb-9982-b94b3b7405d6", "start_date": "1626071052.9234927", "testrun_command": "TestRunCommand2", "testrun_output_dir": "output_dir2", "aut_url": "http://aut_url2", "git_username": "username2", "job_type": "dev", "browser_under_test": [ { "browser_name": "Chrome", "browser_resolution": "840x940", "browser_version": "92.0", "browser_count": 1.0 } ] }, { "end_date": "", "status": "completed", "version_control_url": "http://testUrl1", "testrun_name": "MyApp1", "submit_date": "1626011213.9387646", "testrun_id": "7394b53e-e24e-11eb-bca8-a90c10546d7f", "test_code_s3_object_key": "SampleKey1", "org_id": "6826fcab-e22c-11eb-8abc-b94b3b7405d6", "user_id": "40543869-e22c-11eb-9982-b94b3b7405d6", "start_date": "1626011213.938775", "testrun_command": "TestRunCommand1", "testrun_output_dir": "output_dir1", "aut_url": "http://aut_url1", "git_username": "username1", "job_type": "dev", "browser_under_test": [ { "browser_name": "Chrome", "browser_resolution": "840x940", "browser_version": "91.0", "browser_count": 10.0 } ] } ]

@mock_dynamodb2
def create_dynamodb_tables():
    dynamodb = boto3.client('dynamodb', region_name = 'us-east-1')
    dynamodb.create_table(
        AttributeDefinitions=[
            {
                'AttributeName': 'testrun_id',
                'AttributeType': 'S'
            }
        ],
        TableName= 'testrun',
        KeySchema=[
            {
                'AttributeName': 'testrun_id',
                'KeyType': 'HASH'
            }
        ],
        ProvisionedThroughput={
        'ReadCapacityUnits': 10,
        'WriteCapacityUnits': 10
    }
    )

@mock_dynamodb2
@patch('boto3.resource')
def test_success_response(dynamodb_test):
    #history_details_list = [{'user_id':'100_tf-cfo-17-01','emails':'abc@xyz.com','company_name':'XYZ'}]
    create_dynamodb_tables()
    response = run_handler(EVENT_JSON, None)
    assert response["statusCode"] == 200

@mock_dynamodb2
@patch('boto3.resource')
def test_failure(dynamodb_test):
    #history_details_list = [{'user_id':'100_tf-cfo-17-01','emails':'abc@xyz.com','company_name':'XYZ'}]
    create_dynamodb_tables()
    response = run_handler(EVENT_JSON, None)
    assert response["statusCode"] == 405

